
import WebAPI from './WebAPI.js'
import NativeAPI from './NativeAPI.js'

// const api = NATIVE_API ? new NativeAPI() : new WebAPI()
const api = new WebAPI()

export const getURL = api.getURL
export const sendMessage = api.sendMessage
export const log = api.log
