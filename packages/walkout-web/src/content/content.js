/* global document */

// import ext from '../utils/ext'

// function onRequest (request) {
//   if (request.action === 'change-color') {
//     document.body.style.background = request.data.color
//   }
// }

// ext.runtime.onMessage.addListener(onRequest)

import ComposerButton from './ComposerButton'
import DecryptionFrame from './DecryptionFrame'
import { getURL, sendMessage } from '../api'

// import logger from '../Logger'

/**
 * This class manages interactions between Deface and the Facebook
 * page after it was injected with this script.
 *
 * It tracks changes to the Facebook feed DOM to detect Deface
 * posts and inject DecryptionFrame objects that display resulting plaintext.
 *
 * It also captures new messages submitted by the user to encrypt them
 * before sending them to Facebook.
 */

class ContentScript {
  /**
   * Create a ContentScript.
   */
  constructor () {
    this.composerElement = null
    this.feedElement = null
    this.submitElement = null
    this.contentMutationObserver = null
    this.composerMutationObserver = null
    this.feedMutationObserver = null
    this.postElements = []
    this.textBoxElements = []

    this.decryptionFrames = {}
    this.composerButtons = []
    this.contactTags = []

    this.state = {
      username: null,
      friends: [],
      sentRequests: [],
      requests: []
    }

    this.init = this.init.bind(this)
    this.onDOMMutated = this.onDOMMutated.bind(this)
    this.getNewPosts = this.getNewPosts.bind(this)
    this.onPostUnfolded = this.onPostUnfolded.bind(this)
    this.getTextBoxes = this.getTextBoxes.bind(this)
    this.addContactTag = this.addContactTag.bind(this)
    this.getUpdates = this.getUpdates.bind(this)
  }

  init () {
    const textBoxElements = Array.prototype.slice.call(document.querySelectorAll('*[role="textbox"], textarea'))
    if (textBoxElements.length > 0) {
      this.getTextBoxes([textBoxElements], [])
    }

    const postElements = Array.prototype.slice.call(document.querySelectorAll('*[role="article"], article'))
    if (postElements.length > 0) {
      this.getNewPosts([postElements], [])
    }

    const mutationObserver = new window.MutationObserver(this.onDOMMutated)
    mutationObserver.observe(document.body, { childList: true, attributes: true, subtree: true })

    this.getUpdates()

    return this
  }

  getUpdates () {
    window.chrome.runtime.sendMessage(
      {
        type: 'get-updates'
      },
      (response) => {
        if (response.error) {
          console.error(response.error)
          return window.setTimeout(this.getUpdates, 3000)
        }

        const needsUpdate = Object.keys(response)
          .some(key => JSON.stringify(response[key]) !== JSON.stringify(this.state[key]))

        if (needsUpdate) {
          this.contactTags.forEach(tag => {
            const username = tag.id.split('friend-')[1]
            if (response.friends.indexOf(username) > -1) {
              tag.className = 'code-talker-friend-button friend'
              tag.style.backgroundColor = 'green'
              tag.innerText = 'Code Talker friend'
            } else {
              if (this.state.sentRequests.indexOf(username) > -1) {
                tag.style.backgroundColor = '#aaa'
                tag.innerText = 'Request sent'
                tag.className = 'code-talker-friend-button requested'
              } else {
                tag.className = 'code-talker-friend-button stranger'
                tag.style.backgroundColor = 'red'
                tag.innerText = 'Add on Code Talker'
              }
            }
          })

          Object
            .keys(this.decryptionFrames)
            .forEach(messageId => {
              const frame = this.decryptionFrames[messageId]
              if (!frame.initialized && response.username && (response.friends.indexOf(frame.userId) > -1 || response.username === frame.userId)) {
                frame.init()
              }
            })
        }

        const sentRequests = this.state.sentRequests.filter(friend => response.friends.indexOf(friend) === -1)

        this.state = {
          ...response,
          sentRequests
        }

        window.setTimeout(this.getUpdates, 3000)
      }
    )
  }

  onDOMMutated (mutationsList) {
    // TODO:1
    // attributes could be added when processed as well
    // style can change as well

    const childMutations = mutationsList.filter(mutation => mutation.type === 'childList')
      .map(mutation => Array.prototype.slice.call(mutation.addedNodes).filter(node => node.nodeType === 1))
      .filter(nodes => nodes.length > 0)

    const roleMutations = mutationsList.filter(mutation => mutation.attributeName === 'role')

    this.getTextBoxes(childMutations, roleMutations)
    this.getNewPosts(childMutations, roleMutations)
  }

  getTextBoxes (childMutations, roleMutations) {
    // if (childMutations.length > 0) {
    //   console.log('1', childMutations)
    // }

    const textBoxChildMutations = childMutations.reduce((list, nodes) => {
      nodes
        .map(element => {
          return element.getAttribute('role') === 'textbox' || element.tagName.toLowerCase() === 'textarea'
            ? [element]
            : Array.prototype.slice.call(element.querySelectorAll('*[role="textbox"], textarea'))
        })
        .reduce((list, elements) => {
          elements.forEach(element => {
            list.push(element)
          })
          return list
        })
        .filter(element => element)
        .forEach(element => {
          list.push(element)
        })
      return list
    }, [])

    // if (textBoxChildMutations.length > 0) {
    //   console.log('2', textBoxChildMutations)
    // }

    const textBoxRoleMutations = roleMutations
      .filter(mutation => mutation.target.getAttribute('role') === 'textbox' || mutation.target.tagName.toLowerCase() === 'textarea')
      .map(mutation => mutation.target)

    // if (textBoxRoleMutations.length > 0) {
    //   console.log('3', textBoxRoleMutations)
    // }

    const newTextBoxes = textBoxChildMutations
      .concat(textBoxRoleMutations)
      .filter(element => this.textBoxElements.indexOf(element) === -1)
      .reduce((list, element) => {
        if (list.indexOf(element) === -1) {
          list.push(element)
        }
        return list
      }, [])

    // if (newTextBoxes.length > 0) {
    //   console.log('4', newTextBoxes)
    // }

    newTextBoxes.forEach(element => {
      this.textBoxElements.push(element)
      const composerButton = new ComposerButton().init(element)
      this.composerButtons.push(composerButton)
    })
  }

  getNewPosts (childMutations, roleMutations) {
    const postChildMutations = childMutations.reduce((list, nodes) => {
      const n = nodes
        .map(element => {
          return element.getAttribute('role') === 'article' || element.tagName.toLowerCase() === 'article'
            ? [element]
            : Array.prototype.slice.call(element.querySelectorAll('*[role="article"], article'))
        })
        .reduce((list, elements) => {
          elements.forEach(element => {
            list.push(element)
          })
          return list
        })
        .filter(element => element)

      n.forEach(element => {
        list.push(element)
      })

      return list
    }, [])

    // if (postChildMutations) {
    //   console.log('1', postChildMutations)
    // }

    const postRoleMutations = roleMutations
      .filter(mutation => mutation.target.getAttribute('role') === 'article' || mutation.target.tagName.toLowerCase() === 'article')
      .map(mutation => mutation.target)

    const newPosts = postChildMutations
      .concat(postRoleMutations)
      .filter(element => this.postElements.indexOf(element) === -1)
      .reduce((list, element) => {
        if (list.indexOf(element) === -1) {
          list.push(element)
        }
        return list
      }, [])

    // if (newPosts) {
    //   console.log('2', newPosts)
    // }

    newPosts.forEach(element => {
      // TODO
      const seeMoreButton = element.querySelector('div[data-testid="CometTextWithEntities-seeMoreBtn"]')
      if (seeMoreButton) {
        const mutationObserver = new window.MutationObserver(() => { this.onPostUnfolded(element) })
        mutationObserver.observe(element, { childList: true, attributes: true, subtree: true })
        seeMoreButton.click()
      }
    })
  }

  onPostUnfolded (element) {
    const children = Array.prototype.slice.call(element.querySelectorAll('*'))

    const nestedArticles = Array.prototype.slice.call(element.querySelectorAll('*[role="article"], article'))

    // TODO
    const visibleElements = children.filter(element => true)
    // .filter(element => {
    //   const bbox = element.getBoundingClientRect()
    //   // const isvis = isVisible(element)
    //   // return isvis
    //   // return isVisible(element) && bbox.width >= 2 && bbox.height >= 2
    //   // return true
    //   return bbox.width >= 2 && bbox.height >= 2
    // })

    const visibleNodes = visibleElements.reduce((list, element) => {
      Array.prototype.slice.call(element.childNodes)
        .filter(node => node.nodeType === 3)
        .forEach(node => {
          list.push(node)
        })
      return list
    }, [])

    const rootNodes = nestedArticles.length === 0 ? visibleNodes : visibleNodes
      .filter(node => {
        let n = node
        while (n && n !== element) {
          if (nestedArticles.indexOf(n) > -1) {
            return false
          }
          n = n.parentNode
        }
        return true
      })

    // TODO: facebook could technically split up nodes that appear as unreadable to humans?
    const contentNodes = rootNodes
      .filter(node => node.data.length > 0)

    const nodeCharacterIndices = []

    let i = 0
    contentNodes.forEach(node => {
      nodeCharacterIndices.push(i)
      i += node.data.length + 1
    })

    // TODO
    const content = contentNodes.map(node => node.data).join(' ')
    const regexp = /This\smessage\swas\sencrypted\swith.*?((?:[A-Za-z0-9+/]+?==|[A-Za-z0-9+/]+?=)?);;;([0-9a-zA-Z]{1,40});;;0;;;([0-9a-zA-Z-_]{9})/
    const match = content.match(regexp)

    if (match && match.length === 4) {
      const contentStartIndex = content.indexOf(match[0])
      const contentEndIndex = contentStartIndex + match[0].length

      let contentStartNode = null
      let contentEndNode = null

      for (let i = 0; i < contentNodes.length; i++) {
        if (nodeCharacterIndices[i] > contentStartIndex) {
          contentStartNode = contentNodes[i - 1]
          break
        }
      }

      for (let i = contentNodes.length - 1; i >= 0; i--) {
        if (nodeCharacterIndices[i] <= contentEndIndex) {
          contentEndNode = contentNodes[i]
          break
        }
      }

      const parents = (node) => {
        var nodes = [node]
        for (; node; node = node.parentNode) {
          nodes.unshift(node)
        }
        return nodes
      }

      // TODO
      let defaceElement = null
      var parents1 = parents(contentStartNode)
      var parents2 = parents(contentEndNode)
      for (let i = 0; i < parents1.length; i++) {
        if (parents1[i] !== parents2[i]) {
          defaceElement = parents1[i - 1]
          break
        }
      }

      const messageId = match[3]
      let decryptionFrame = this.decryptionFrames[messageId]

      if (!decryptionFrame) {
        const userId = match[2]
        const isSelf = this.state.username === userId
        const isFriend = this.state.friends.indexOf(userId) > -1

        if (this.state.username && this.state.username !== userId) {
          this.addContactTag(defaceElement, userId, isFriend)
        }

        const content = match[0]
        const id = Object.keys(this.decryptionFrames).length
        decryptionFrame = new DecryptionFrame(id, defaceElement, content, userId)
        this.decryptionFrames[messageId] = decryptionFrame

        if (this.state.username && (isFriend || isSelf)) {
          decryptionFrame.init()
        }
      }
    }

    this.postElements.push(element)
  }

  addContactTag (defaceElement, userId, isFriend) {
    const profileBarElement = defaceElement.closest('div[role="article"]').querySelector('.j5wam9gi.knj5qynh.m9osqain.hzawbc8m')

    const friendButton = document.createElement('div')
    friendButton.className = 'code-talker-friend-button stranger'
    friendButton.id = `friend-${userId}`
    friendButton.innerText = 'Add on Code Talker'
    friendButton.style.display = 'inline-block'
    friendButton.style.marginLeft = '10px'
    friendButton.style.borderRadius = '3px'
    friendButton.style.backgroundColor = 'red'
    friendButton.style.color = 'white'
    friendButton.style.cursor = 'pointer'
    profileBarElement.appendChild(friendButton)

    if (this.contactTags.indexOf(friendButton) === -1) {
      this.contactTags.push(friendButton)
    }

    if (isFriend) {
      friendButton.className = 'code-talker-friend-button friend'
      friendButton.style.backgroundColor = 'green'
      friendButton.innerText = 'Code Talker friend'
    }

    friendButton.addEventListener('click', () => {
      sendMessage(
        {
          type: 'request-friend',
          userId
        },
        (response) => {
          if (!response) {
            return console.error('empty response')
          }

          if (response.error && response.error !== 'request was already sent') {
            return console.error(response.error)
          }

          friendButton.style.backgroundColor = '#aaa'
          friendButton.innerText = 'Request sent'
          friendButton.className = 'code-talker-friend-button requested'

          if (this.state.sentRequests.indexOf(userId) === -1) {
            this.state.sentRequests.push(userId)
          }
        }
      )
    })
  }
}

new ContentScript().init()
