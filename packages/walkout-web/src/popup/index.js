/* global document */

import React from 'react'
import ReactDOM from 'react-dom'
import Popup from './Popup.js'

const Index = () => <Popup />

ReactDOM.render(<Index />, document.getElementById('display-container'))
