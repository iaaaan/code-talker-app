
import { sendMessage } from '../api'
// import DecryptionSandbox from '../decryptionsandbox/index.js'

// import logger from '../Logger'

/**
 * This class manages the container element for Deface posts. It parses
 * content from Facebook posts, and renders an iframe that contains the
 * necessary parameters for the background side to decrypt messages.
 *
 * Decryption-sandbox.html points to src/decryption-sandbox/index.html.
 */
export default class DecryptionFrame {
  /**
   * Create a DecryptionFrame.
   */
  constructor (id, element, content, userId) {
    this.id = id
    this.element = element
    this.content = content
    this.userId = userId
    this.initialized = false

    const regexp = /((?:[A-Za-z0-9\+/]+?==|[A-Za-z0-9\+/]+?=?)?);;;([0-9a-zA-Z]{1,40});;;0;;;([0-9a-zA-Z-_]{9})/
    const match = this.content.match(regexp)
    this.messageId = match[3]
    this.codeTalkerMessage = `${match[1]};;;${match[2]};;;0;;;${match[3]}`

    this.init = this.init.bind(this)
    this.renderFrame = this.renderFrame.bind(this)
    console.log('FRAME', this.content, this.codeTalkerMessage)
  }

  init () {
    if (this.initialized) {
      return null
    }

    this.initialized = true

    sendMessage(
      {
        type: 'get-decryption-sandbox-size',
        id: this.id
      },
      (response) => {
        if (!response) {
          return console.error('empty response')
        }

        const {
          height,
          error
        } = response

        if (error || typeof height !== 'number') {
          return console.error(error || 'Invalid frame size')
        }

        this.frame.style.height = `${height}px`
        this.dialog.style.height = `${height}px`
      }
    )

    this.renderFrame(this.id)

    return this
  }

  /**
   * Render a container element sized as the initial cipherText post,
   * and insert an iframe that points to src/decryption-sandbox/index.html.
   */
  renderFrame (id) {
    const boundingBox = this.element.getBoundingClientRect()

    this.frame = document.createElement('div')
    this.frame.setAttribute('id', `frame-${this.keyHash}`)
    this.frame.className = 'decryption-frame'
    this.frame.style.width = `${boundingBox.width}px`
    this.frame.style.height = `${boundingBox.height}px`
    // this.frame.style.transition = 'height 0.15s'

    this.element.innerHTML = ''
    this.element.appendChild(this.frame)

    this.dialog = document.createElement('iframe')
    this.dialog.setAttribute('id', `dialog-${this.keyHash}`)
    this.dialog.className = 'decryption-frame-dialog'
    this.dialog.setAttribute('frameBorder', 0)
    this.dialog.setAttribute('scrolling', 'no')
    this.dialog.style.border = 'none'
    this.dialog.style.width = '100%'
    this.dialog.style.height = `${boundingBox.height}px`
    // this.dialog.style.transition = 'height 0.15s'

    const params = window.encodeURI(`id=${this.id}&messageId=${this.messageId}&codeTalkerMessage=${this.codeTalkerMessage}`)

    const url = window.chrome.runtime.getURL(`decryptionsandbox/index.html?${params}`)
    this.dialog.setAttribute('src', url)
    this.frame.appendChild(this.dialog)
  }
}
