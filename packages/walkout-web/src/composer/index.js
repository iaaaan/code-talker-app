
import React from 'react'
import ReactDOM from 'react-dom'
import Composer from './Composer'

const query = new URLSearchParams(document.location.search)
const requestId = query.get('requestId')

ReactDOM.render(
  (
    <Composer
      requestId={requestId}
    />
  ),
  document.getElementById('display-container')
)
