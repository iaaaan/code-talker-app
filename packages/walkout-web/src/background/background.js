'use strict'

import async from 'async'
import KeychainManager from './KeychainManager.js'

import '../_img/icon-16.png'
import '../_img/icon-19.png'
import '../_img/icon-38.png'
import '../_img/icon-48.png'
import '../_img/icon-128.png'

class Deface {
  constructor () {
    this.composerRequestIndex = 0
    this.composerRequests = {}
    this.sizeRequests = {}

    this.keychainManager = new KeychainManager()
    this.onMessage = this.onMessage.bind(this)
    this.openComposer = this.openComposer.bind(this)
    this.submit = this.submit.bind(this)
    this.addDecryptionSandboxSizeRequest = this.addDecryptionSandboxSizeRequest.bind(this)
    this.resizeDecryptionFrame = this.resizeDecryptionFrame.bind(this)
    this.exitComposer = this.exitComposer.bind(this)
    this.checkFriendsList = this.checkFriendsList.bind(this)
    this.requestFriend = this.requestFriend.bind(this)
    this.setUsername = this.setUsername.bind(this)
    this.getUsername = this.getUsername.bind(this)
    this.getUpdates = this.getUpdates.bind(this)
    this.signin = this.signin.bind(this)
    this.signup = this.signup.bind(this)
    this.signout = this.signout.bind(this)

    window.chrome.runtime.onMessage.addListener(this.onMessage)

    this.getUpdates((data) => {
      if (!data) {
        return console.error('empty response')
      }

      if (data.error) {
        if (data.error === 'user needs to sign in') {
          window.chrome.tabs.create({ url: 'chrome://extensions/?options=' + window.chrome.runtime.id })
          return null
        } else {
          return console.error(data.error)
        }
      }

      this.username = data.username
    })
  }

  onMessage (request, sender, callback) {
    // TODO check for sender.tab.url.indexOf('facebook.com') > -1
    switch (request.type) {
      case 'decrypt' :
        this.keychainManager.addRequestToQueue(request.codeTalkerMessage, sender.tab.id, callback)
        break
      case 'composer' :
        this.openComposer(sender.tab.id, callback)
        break
      case 'submit' :
        this.submit(request.plainText, request.requestId, callback)
        break
      case 'exit-composer' :
        this.exitComposer(request.requestId, callback)
        break
      case 'get-decryption-sandbox-size' :
        this.addDecryptionSandboxSizeRequest(sender.tab.id, request.id, callback)
        break
      case 'resize-decryption-frame' :
        this.resizeDecryptionFrame(sender.tab.id, request.id, request.height)
        break
      case 'check-friends-list' :
        this.checkFriendsList(request.userId, callback)
        break
      case 'request-friend' :
        this.requestFriend(request.userId, callback)
        break
      case 'set-username' :
        this.setUsername(request.username)
        break
      case 'get-username' :
        this.getUsername(callback)
        break
      case 'get-updates' :
        this.getUpdates(callback)
        break
      case 'signin' :
        this.signin(request.data, callback)
        break
      case 'signup' :
        this.signup(request.data, callback)
        break
      case 'signout' :
        this.signout(callback)
        break
      default : {
        const emptyResponse = {}
        callback(emptyResponse)
      }
    }
    return true
  }

  signin (data, callback) {
    window
      .fetch(
        'http://localhost:3000/signin',
        {
          method: 'POST',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }
      )
      .then(response => response.json())
      .then(response => {
        if (response.status === 'error') {
          return console.error(response.error)
        }

        this.username = response.data.username
        console.log(response)
        callback(response)
      })
  }

  signup (data, callback) {
    window
      .fetch(
        'http://localhost:3000/signup',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }
      )
      .then(response => response.json())
      .then(response => {
        if (response.status === 'error') {
          return console.error(response.error)
        }

        this.username = response.data.username
        callback(response)
      })
  }

  signout (callback) {
    window
      .fetch(
        'http://localhost:3000/signout',
        {
          method: 'GET'
        }
      )
      .then(response => response.json())
      .then(response => {
        console.log('signout', response)
        if (response.status === 'error') {
          return console.error(response.error)
        }

        this.username = null
        callback(response)
      })
  }

  getUpdates (callback) {
    async.mapSeries(
      ['me', 'friends', 'requests'],
      (endpoint, callback) => {
        const url = `http://localhost:3000/${endpoint}`

        window
          .fetch(url, { method: 'GET' })
          .then(response => response.json())
          .then(response => {
            if (response.error) {
              return callback(response.error)
            }

            callback(null, response.data)
          })
      },
      (error, results) => {
        if (error) {
          const response = { error }
          return callback(response)
        }

        const resultsMap = results.reduce(
          (map, result) => {
            return {
              ...map,
              ...result
            }
          },
          {}
        )

        if (callback) {
          callback(resultsMap)
        }
      }
    )
  }

  setUsername (username) {
    this.username = username
  }

  getUsername (callback) {
    const response = { username: this.username }
    callback(response)
  }

  requestFriend (username, callback) {
    const data = {
      username
    }

    window
      .fetch(
        'http://localhost:3000/request',
        {
          method: 'POST',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }
      )
      .then(data => data.json())
      .then(data => {
        console.log(data)
        const response = {
          error: data.error
        }

        callback(response)
      })
  }

  checkFriendsList (userId, callback) {
    window
      .fetch(
        'http://localhost:3000/friends',
        {
          method: 'GET'
        }
      )
      .then(data => data.json())
      .then(data => {
        if (data.status === 'error') {
          const response = {
            error: data.error
          }

          return callback(response)
        }

        const isFriend = data.data.friends.indexOf(userId) > -1
        const response = {
          isFriend
        }

        callback(response)
      })
  }

  addDecryptionSandboxSizeRequest (tabId, frameId, callback) {
    const id = `${tabId}-${frameId}`
    this.sizeRequests[id] = callback
  }

  resizeDecryptionFrame (tabId, frameId, height) {
    const id = `${tabId}-${frameId}`
    const callback = this.sizeRequests[id]

    if (callback) {
      try {
        window.chrome.tabs.get(tabId, tab => {
          if (tab) {
            const response = { height }
            callback(response)
          }
        })
      } catch (error) {}
    }
  }

  openComposer (tabId, callback) {
    const width = 640
    const height = 480
    const top = 50
    const left = 50
    const type = 'popup'

    this.composerRequests[this.composerRequestIndex] = callback

    const params = `requestId=${this.composerRequestIndex}`
    const url = window.chrome.runtime.getURL(`composer/index.html?${params}`)

    window.chrome.windows.create({ url, width, height, top, left, type, tabId })

    this.composerRequestIndex++
  }

  submit (plainText, requestId, callback) {
    this.keychainManager.encryptAndStore(plainText, (error, data) => {
      const {
        codeTalkerMessage
      } = data

      const response = {
        error,
        codeTalkerMessage
      }

      if (this.composerRequests[requestId]) {
        this.composerRequests[requestId](response)
        delete this.composerRequests[requestId]
        callback(response)
      } else {
        callback(response)
      }
    })
  }

  exitComposer (requestId, callback) {
    if (this.composerRequests[requestId]) {
      const response = {}
      this.composerRequests[requestId](response)
      delete this.composerRequests[requestId]
    }

    callback()
  }
}

const deface = new Deface()
