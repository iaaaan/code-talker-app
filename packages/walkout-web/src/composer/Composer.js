
import React, { Component } from 'react'
import patchIcon from '../_img/icon-256.png'
import cautionIcon from '../_img/caution.svg'
import './styles.css'

export default class Composer extends Component {
  constructor (props) {
    super(props)

    this.state = {
      defaultPostContent: 'What\'s on your mind?',
      postContent: 'What\'s on your mind?',
      encryptedContent: null,
      complexity: 1,
      alpha: 0,
      speed: 0.15,
      textAreaHeight: 112
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.animate = this.animate.bind(this)
    this.exit = this.exit.bind(this)
  }

  componendDidMount () {
    const documentSize = document.body.getBoundingClientRect()
    window.resizeTo(documentSize.width, documentSize.height + 20)
  }

  handleSubmit () {
    const {
      postContent,
      defaultPostContent
    } = this.state

    if (!postContent || postContent === defaultPostContent) {
      return null
    }

    const params = {
      type: 'submit',
      requestId: this.props.requestId,
      plainText: postContent
    }

    window.chrome.runtime.sendMessage(
      params,
      (response) => {
        if (!response) {
          console.error('empty response')
          return window.close()
        }

        const {
          error,
          codeTalkerMessage
        } = response

        if (error || !codeTalkerMessage) {
          console.error(error)
          return this.exit()
        }

        this.setState(
          {
            ...this.state,
            encryptedContent: `This message was encrypted with Code Talker:\n ${codeTalkerMessage}`
          },
          () => {
            this.animate()
          }
        )
      }
    )
  }

  animate () {
    const {
      encryptedContent,
      speed
    } = this.state

    let {
      alpha,
      postContent
    } = this.state

    const missingCharacterCount = encryptedContent.length - postContent.length

    if (missingCharacterCount > 0) {
      for (let i = 0; i < missingCharacterCount; i++) {
        if (Math.random() < alpha / 2) {
          postContent += Math.random().toString(36).slice(2)[0]
        }
      }
    }

    const text = Array.prototype.slice.call(postContent)
      .map((c, i) => {
        if (encryptedContent.charAt(i) === c || Math.random() < alpha) {
          return encryptedContent.charAt(i)
        }
        return Math.random().toString(36).slice(2)[0]
      })
      .join('')

    alpha = (1 - alpha) * speed
    postContent = text

    this.setState(
      {
        ...this.state,
        alpha,
        postContent
      },
      () => {
        if (postContent === encryptedContent) {

        } else {

        }
      }
    )

    if (text === encryptedContent) {
      window.setTimeout(() => {
        document.body.style.opacity = 0
        window.setTimeout(() => {
          window.close()
        }, 750)
      }, 750)
    } else {
      window.requestAnimationFrame(this.animate)
    }
  }

  exit () {
    const params = {
      type: 'exit-composer',
      requestId: this.props.requestId
    }

    window.chrome.runtime.sendMessage(
      params,
      () => {
        window.close()
      }
    )
  }

  render () {
    const {
      postContent,
      textAreaHeight,
      defaultPostContent,
      encryptedContent
    } = this.state

    return (
      <div
        style={{
          pointerEvents: encryptedContent ? 'none' : 'auto'
        }}
      >
        <div id='header'>
          <h1>
            Create an encrypted post with Code Talker
          </h1>
          <div id='exit-button'>
            <svg viewport='0 0 9 9' version='1.1' xmlns='http://www.w3.org/2000/svg'>
              <line
                x1={1}
                y1={8}
                x2={8}
                y2={1}
                stroke='rgb(96, 100, 115)'
                strokeWidth={2}
              />
              <line
                x1={1}
                y1={1}
                x2={8}
                y2={8}
                stroke='rgb(96, 100, 115)'
                strokeWidth={2}
              />
            </svg>
          </div>
        </div>
        <div id='post'>
          <div
            id='patch'
            style={{
              backgroundImage: `url('${window.chrome.runtime.getURL(patchIcon)}')`
            }}
          />
          <textarea
            id='composer'
            ref='composer'
            value={postContent}
            onChange={(event) => {
              this.setState({
                ...this.state,
                postContent: event.target.value,
                textAreaHeight: this.refs.composer.scrollHeight
              })
            }}
            onFocus={(event) => {
              if (postContent === defaultPostContent) {
                this.setState({
                  ...this.state,
                  postContent: ''
                })
              }
            }}
            onBlur={(event) => {
              if (postContent === '') {
                this.setState({
                  ...this.state,
                  postContent: defaultPostContent
                })
              }
            }}
            style={{
              height: textAreaHeight,
              color: postContent === defaultPostContent ? '#90949c' : 'black',
              fontSize: postContent.length < 85 ? 24 : 14,
              lineHeight: postContent.length < 85 ? '28px' : '18px'
            }}
          />
        </div>
        <div id='settings'>
          <div id='settings-wrapper'>
            <div id='contacts'>
              <div className='icons'>
                <div className='portrait' />
                <div className='portrait' />
                <div className='portrait' />
              </div>
              <p>
                This post will be readable by ---- and 0 other friends
              </p>
            </div>
            <div id='caution'>
              <div className='icon'>
                <img src={window.chrome.runtime.getURL(cautionIcon)}/>
              </div>
              <p>
                Using Code Talker involves some risk. Please <a href='#'>learn more</a>.
              </p>
            </div>
          </div>
        </div>
        <div
          id='submit'
          style={{
            opacity: postContent && postContent !== defaultPostContent ? 1 : 0.4,
            pointerEvents: postContent && postContent !== defaultPostContent ? 'auto' : 'none',
            cursor: postContent && postContent !== defaultPostContent ? 'pointer' : 'auto',
          }}
          onClick={this.handleSubmit}
        >
          Share
        </div>
      </div>
    )
  }
}
