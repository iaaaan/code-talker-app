
import React, { Component } from 'react'

export default class ContactList extends Component {
  constructor (props) {
    super(props)

    this.state = {
      friends: null,
      requests: null
    }

    this.getContacts = this.getContacts.bind(this)
    this.acceptRequest = this.acceptRequest.bind(this)
    this.rejectRequest = this.rejectRequest.bind(this)
    this.deleteFriend = this.deleteFriend.bind(this)
    this.clear = this.clear.bind(this)
  }

  componentDidMount () {
    this.getContacts()
  }

  clear () {
    this.setState({
      ...this.state,
      friends: null,
      requests: null
    })
  }

  getContacts () {
    window
      .fetch(
        'http://localhost:3000/friends',
        {
          method: 'GET',
          mode: 'cors'
        }
      )
      .then(response => response.json())
      .then(response => {
        if (response.status === 'error') {
          return console.error(response.error)
        }

        this.setState({
          ...this.state,
          friends: response.data.friends
        })

        window
          .fetch(
            'http://localhost:3000/requests',
            {
              method: 'GET',
              mode: 'cors'
            }
          )
          .then(response => response.json())
          .then(response => {
            if (response.status === 'error') {
              return console.error(response.error)
            }

            this.setState({
              ...this.state,
              requests: response.data.requests
            })
          })
      })
  }

  acceptRequest (id) {
    const data = {
      username: this.state.requests[id]
    }

    window
      .fetch(
        'http://localhost:3000/accept',
        {
          method: 'POST',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }
      )
      .then(response => response.json())
      .then(response => {
        if (response.status === 'error') {
          return console.error(response.error)
        }

        this.setState({
          ...this.state,
          friends: response.data.friends,
          requests: response.data.requests
        })
      })
  }

  rejectRequest (id) {
    const data = {
      username: this.state.requests[id]
    }

    window
      .fetch(
        'http://localhost:3000/reject',
        {
          method: 'POST',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }
      )
      .then(response => response.json())
      .then(response => {
        if (response.status === 'error') {
          return console.error(response.error)
        }

        this.setState({
          ...this.state,
          requests: response.data.requests
        })
      })
  }

  deleteFriend (id) {
    const data = {
      username: this.state.friends[id]
    }

    window
      .fetch(
        'http://localhost:3000/delete',
        {
          method: 'POST',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }
      )
      .then(response => response.json())
      .then(response => {
        // console.log(response)
        // if (response.status === 'error') {
        //   return console.error(response.error)
        // }

        this.getContacts()
        // this.setState({
        //   ...this.state,
        //   friends: response.data.friends
        // })
      })
  }

  render () {
    const {
      friends,
      requests
    } = this.state

    const friendList = !friends ? (<div>Loading friends...</div>)
      : friends.length === 0 ? (<div style={{ opacity: 0.5 }}>No friend :(</div>)
        : friends.map((friend, i) => {
          return (
            <li
              className='friend'
              key={`friend-${i}`}
              ref={`friend-${i}`}
              style={{
                display: 'flex',
                justifyContent: 'space-between'
              }}
            >
              <div>
                <span>{friend}</span>
                <span
                  style={{
                    marginLeft: 5,
                    opacity: 0.5
                  }}
                >
                  • unverified
                </span>
              </div>
              <div
                className='controls'
              >
                <div
                  className='button verify'
                >
                  <span>
                    ✓
                  </span>
                </div>
                <div
                  className='button delete'
                  onClick={() => this.deleteFriend(i)}
                >
                  <span>
                    ×
                  </span>
                </div>
              </div>
            </li>
          )
        })

    const requestList = !requests ? (<div>Loading requests...</div>)
      : requests.length === 0 ? (<div style={{ opacity: 0.5 }}>No friend request</div>)
        : requests.map((request, i) => {
          return (
            <li
              key={`request-${i}`}
              ref={`request-${i}`}
              style={{
                display: 'flex',
                justifyContent: 'space-between'
              }}
            >
              <div>
                {request}
              </div>
              <div
                className='controls'
              >
                <div
                  className='button delete'
                  onClick={() => this.rejectRequest(i)}
                >
                  <span>
                    ×
                  </span>
                </div>
                <div
                  className='button verify'
                  onClick={() => this.acceptRequest(i)}
                >
                  <span>
                    ✓
                  </span>
                </div>
              </div>
            </li>
          )
        })

    return (
      <div
        id='contact-list'
      >
        <div id='requests'>
          <h3>
            Friend Requests
          </h3>
          <ul
            style={{
              listStyle: 'none',
              padding: 0
            }}
          >
            {requestList}
          </ul>
        </div>
        <div id='friends'>
          <div className='title'>
            <h3>
              Friends List
            </h3>
            <a href='#'>
              <div>See all</div>
            </a>
          </div>
          <ul
            style={{
              listStyle: 'none',
              padding: 0
            }}
          >
            {friendList}
          </ul>
        </div>
      </div>
    )
  }
}
