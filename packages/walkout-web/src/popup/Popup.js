import React, { Component } from 'react'
import ContactList from './ContactList.js'
import patchIcon from '../_img/icon-256.png'
import './styles.css'

export default class Popup extends Component {
  constructor (props) {
    super(props)
    this.state = {
      initialized: false,
      signinFocused: true,
      username: null,
      signinForm: {
        username: '',
        password: ''
      },
      signupForm: {
        username: '',
        email: '',
        password: '',
        verificaion: ''
      }
    }

    this.handleSignin = this.handleSignin.bind(this)
    this.handleSignup = this.handleSignup.bind(this)
    this.handleSignout = this.handleSignout.bind(this)
    this.handlePanelToggle = this.handlePanelToggle.bind(this)
  }

  componentDidMount () {
    window.chrome.runtime.sendMessage(
      {
        type: 'get-username'
      },
      (response) => {
        if (!response) {
          return console.error('empty response')
        }

        this.setState({
          ...this.state,
          initialized: true,
          username: response.username,
          signinFocused: true
        })
      }
    )
  }

  handlePanelToggle () {
    this.setState({
      ...this.state,
      signinFocused: !this.state.signinFocused
    })
  }

  handleSignin (event) {
    const data = {}
    Array
      .from(this.refs['signin-form'].querySelectorAll('input'))
      .forEach(tag => {
        data[tag.getAttribute('name')] = tag.value
      })

    window.chrome.runtime.sendMessage(
      {
        type: 'signin',
        data
      },
      (response) => {
        console.log(response)
        if (response.error) {
          return console.error(response.error)
        }

        this.setState({
          ...this.state,
          username: response.data.username
        })
      }
    )
  }

  handleSignup (event) {
    const data = {}
    Array
      .from(this.refs['signup-form'].querySelectorAll('input'))
      .forEach(tag => {
        data[tag.getAttribute('name')] = tag.value
      })

    window.chrome.runtime.sendMessage(
      {
        type: 'signup',
        data
      },
      (response) => {
        if (response.error) {
          return console.error(response.error)
        }

        this.setState({
          ...this.state,
          username: response.data.username
        })
      }
    )
  }

  handleSignout (event) {
    window.chrome.runtime.sendMessage(
      {
        type: 'signout'
      },
      (response) => {
        if (!response) {
          return console.error('empty response')
        }

        console.log(response)
        if (response.error) {
          return console.error(response.error)
        }

        this.setState({
          ...this.state,
          initialized: true,
          username: null,
          signinFocused: true
        }, () => {
          this.refs['contact-list']
        })
      }
    )
  }

  render () {
    const {
      initialized,
      username,
      signinFocused
    } = this.state

    if (!initialized) {
      return (
        <div>
          Loading...
        </div>
      )
    }

    if (!username) {
      if (signinFocused) {
        return (
          <div>
            <h2>
              Sign in
            </h2>
            <form ref='signin-form'>
              <div>
                <label name='username'>
                  username
                </label>
                <input
                  type='text'
                  name='username'
                  onChange={(event) => {
                    this.setState({
                      ...this.state,
                      signinForm: {
                        ...this.state.signinForm,
                        username: event.target.value
                      }
                    })
                  }}
                  value={this.state.signinForm.username}
                />
              </div>
              <div>
                <label name='password'>
                  password
                </label>
                <input
                  type='password'
                  name='password'
                  onChange={(event) => {
                    this.setState({
                      ...this.state,
                      signinForm: {
                        ...this.state.signinForm,
                        password: event.target.value
                      }
                    })
                  }}
                  value={this.state.signinForm.password}
                />
              </div>
              <div
                className='button'
                onClick={this.handleSignin}
              >
                Sign in
              </div>
              <div
                className='button'
                onClick={this.handlePanelToggle}
              >
                or Sign up
              </div>
            </form>
          </div>
        )
      } else {
        return (
          <div>
            <h2>
              Sign up
            </h2>
            <form ref='signup-form'>
              <div>
                <label name='username'>
                  username
                </label>
                <input
                  type='text'
                  name='username'
                  onChange={(event) => {
                    this.setState({
                      ...this.state,
                      signupForm: {
                        ...this.state.signupForm,
                        username: event.target.value
                      }
                    })
                  }}
                  value={this.state.signupForm.username}
                />
              </div>
              <div>
                <label name='email'>
                  email
                </label>
                <input
                  type='text'
                  name='email'
                  onChange={(event) => {
                    this.setState({
                      ...this.state,
                      signupForm: {
                        ...this.state.signupForm,
                        email: event.target.value
                      }
                    })
                  }}
                  value={this.state.signupForm.email}
                />
              </div>
              <div>
                <label name='password'>
                  password
                </label>
                <input
                  type='password'
                  name='password'
                  onChange={(event) => {
                    this.setState({
                      ...this.state,
                      signupForm: {
                        ...this.state.signupForm,
                        password: event.value
                      }
                    })
                  }}
                  value={this.state.signupForm.password}
                />
              </div>
              <div>
                <label name='verification'>
                  verify password
                </label>
                <input
                  type='password'
                  name='verification'
                  onChange={(event) => {
                    this.setState({
                      ...this.state,
                      signupForm: {
                        ...this.state.signupForm,
                        verification: event.value
                      }
                    })
                  }}
                  value={this.state.signupForm.verification}
                />
              </div>
              <div
                className='button'
                onClick={this.handleSignup}
              >
                Sign up
              </div>
              <div
                className='button'
                onClick={this.handlePanelToggle}
              >
                or Sign in
              </div>
            </form>
          </div>
        )
      }
    }

    return (
      <div>
        <div
          id='header'
        >
          <div id='header-columns'>
            <div id='patch'>
              <img
                src={window.chrome.runtime.getURL(patchIcon)}
                height={128}
                width={128}
              />
            </div>
            <div id='profile'>
              <h2>
                Code Talker
              </h2>
              <p>
                <span>{username}</span>
                <span
                  onClick={this.handleSignout}
                >
                  Sign out
                </span>
              </p>
            </div>
          </div>
        </div>
        <ContactList
          ref='contact-list'
          username={username}
        />
      </div>
    )
  }
}
