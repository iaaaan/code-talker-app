/* global chrome */

const filesInDirectory = dir => {
  return new Promise(resolve => {
    return dir.createReader()
      .readEntries(entries => {
        return Promise
          .all(
            entries
              .filter(e => e.name[0] !== '.')
              .map(e => {
                if (e.isDirectory) {
                  return filesInDirectory(e)
                } else {
                  const resolve = resolvePromise => e.file(resolvePromise)
                  return new Promise(resolve)
                }
              })
          )
          .then(files => [].concat(...files))
          .then(resolve)
      })
  })
}

const timestampForFilesInDirectory = dir =>
  filesInDirectory(dir).then(files =>
    files.map(f => f.name + f.lastModifiedDate).join())

const reload = () => {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    if (tabs[0]) {
      chrome.tabs.reload(tabs[0].id)
    }

    chrome.runtime.reload()
  })
}

const watchChanges = (dir, lastTimestamp) => {
  timestampForFilesInDirectory(dir).then((timestamp) => {
    if (!lastTimestamp || (lastTimestamp === timestamp)) {
      setTimeout(() => watchChanges(dir, timestamp), 1000)
    } else {
      reload()
    }
  })
}

if (chrome) {
  chrome.management.getSelf((self) => {
    if (self.installType === 'development') {
      chrome.runtime.getPackageDirectoryEntry(dir => watchChanges(dir))
    }
  })
}
