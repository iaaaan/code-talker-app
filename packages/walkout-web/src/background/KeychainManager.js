
import { waterfall, filter } from 'async'
import { encrypt, decrypt } from './crypto.js'

export default class KeychainManager {
  constructor () {
    this.storageRequestQueue = []
    this.decryptionRequestQueue = []

    this.isAStorageRequestProcessing = false
    this.isADecryptionRequestProcessing = false

    this.encryptAndStore = this.encryptAndStore.bind(this)
    this.addRequestToQueue = this.addRequestToQueue.bind(this)
    this.processNextStorageRequest = this.processNextStorageRequest.bind(this)
    this.processNextDecryptionRequest = this.processNextDecryptionRequest.bind(this)
    this.queryStorage = this.queryStorage.bind(this)
    this.addKeyToStorage = this.addKeyToStorage.bind(this)
    this.getActiveContentScripts = this.getActiveContentScripts.bind(this)
  }

  encryptAndStore (plainText, callback) {
    waterfall(
      [
        (callback) => encrypt(plainText, callback),
        this.addKeyToStorage
      ],
      callback
    )
  }

  addRequestToQueue (codeTalkerMessage, tabId, callback) {
    const allRequests = this.storageRequestQueue.concat(this.decryptionRequestQueue)
    const messageId = codeTalkerMessage.split(';;;')[3]
    // TODO
    // const request = allRequests.find(request => request.messageId === messageId)
    const request = null

    if (!request) {
      const script = {
        tabId,
        callback
      }

      const request = {
        messageId,
        codeTalkerMessage,
        completed: false,
        scripts: [script]
      }

      console.log('adding request', messageId, codeTalkerMessage)

      this.storageRequestQueue.push(request)
    } else {
      const script = {
        tabId,
        callback
      }

      request.scripts.push(script)
    }

    this.processNextStorageRequest()
  }

  processNextStorageRequest () {
    if (this.isAStorageRequestProcessing) {
      return null
    }

    const request = this.storageRequestQueue.pop()

    if (!request) {
      return null
    }

    this.isAStorageRequestProcessing = true
    console.log('processing storage request', request.messageId)

    waterfall(
      [
        (callback) => this.queryStorage(request, callback),
        (data, callback) => {
          console.log('wut?', request.messageId)
          this.getActiveContentScripts(request, (error, activeScripts) => {
            if (error) {
              console.error(error)
            }

            activeScripts.forEach((script, i) => {
              script.callback(data)
            })

            callback()
          })
        }
      ],
      (error) => {
        console.log('error?', request.messageId)
        if (error) {
          this.decryptionRequestQueue.push(request)
          this.processNextDecryptionRequest()
        }

        this.isAStorageRequestProcessing = false
        this.processNextStorageRequest()
      }
    )
  }

  processNextDecryptionRequest () {
    if (this.isADecryptionRequestProcessing) {
      return null
    }

    const request = this.decryptionRequestQueue.pop()

    if (!request) {
      return null
    }

    this.isADecryptionRequestProcessing = true
    console.log('processing decryption request', request.messageId)
    // console.log('processing brute force request', request.message)

    waterfall(
      [
        (callback) => this.getActiveContentScripts(request, callback),
        (activeScripts, callback) => {
          if (activeScripts.length === 0) {
            this.isADecryptionRequestProcessing = false
            return this.processNextDecryptionRequest()
          }

          decrypt(request, callback)
        },
        this.addKeyToStorage
      ],
      (error, data) => {
        console.log(error, data)
        const plainText = data ? data.plainText : null

        this.getActiveContentScripts(request, (err, activeScripts) => {
          if (err) {
            console.error(err)
          }

          activeScripts.forEach(script => {
            script.callback({
              error,
              plainText
            })

            this.isADecryptionRequestProcessing = false
            this.processNextDecryptionRequest()
          })
        })
      }
    )
  }

  queryStorage (request, callback) {
    const {
      messageId
    } = request

    window.chrome.storage.sync.get(messageId, storeValue => {
      const data = {
        plainText: '',
        read: true
      }


      if (typeof storeValue[messageId] === 'string') {
        data.plainText = storeValue[messageId]
      } else if (typeof storeValue[messageId] === 'object') {
        data.plainText = storeValue[messageId].plainText
        data.read = storeValue[messageId].read
      }

      console.log(data)

      if (!data.plainText) {
        return callback(new Error('No plainText in storage.'))
      } else {
        const storeValue = {
          [messageId]: {
            plainText: data.plainText,
            read: true
          }
        }

        window.chrome.storage.sync.set(storeValue)
      }

      callback(null, data)
    })
  }

  addKeyToStorage (data, callback) {
    const {
      messageId,
      plainText
    } = data

    const storeValue = {
      [messageId]: {
        plainText,
        read: false
      }
    }

    window.chrome.storage.sync.set(storeValue, (error) => {
      if (error) {
        console.error(error)
      }

      callback(null, data)
    })
  }

  getActiveContentScripts (request, callback) {
    filter(
      request.scripts,
      (script, callback) => {
        // TODO
        return callback(null, true)
        // try {
        //   window.chrome.tabs.get(script.tabId, tab => {
        //     if (!tab) {
        //       return callback(null, false)
        //     }
        //     return callback(null, true)
        //   })
        // } catch (error) {
        //   return callback(null, false)
        // }
      },
      callback
    )
  }
}
