
export function encrypt (plainText, callback) {
  const data = {
    plainText
  }

  window
    .fetch(
      'http://localhost:3000/encrypt',
      {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    )
    .then(response => response.json())
    .then(response => {
      if (response.status === 'error') {
        return console.error(response.error)
      }

      const codeTalkerMessage = response.data.codeTalkerMessage
      const messageId = codeTalkerMessage.split(';;;')[3]

      // TODO update
      const responseData = {
        codeTalkerMessage,
        messageId,
        plainText
      }

      callback(null, responseData)
    })
}

export function decrypt (request, callback) {
  const {
    codeTalkerMessage,
    messageId
  } = request

  const data = {
    codeTalkerMessage
  }

  window
    .fetch(
      'http://localhost:3000/decrypt',
      {
        method: 'POST',
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    )
    .then(response => response.json())
    .then(response => {
      console.log(response)
      if (response.status === 'error') {
        return callback(response.error)
      }

      const plainText = response.data.plainText

      const responseData = {
        messageId,
        plainText
      }

      callback(null, responseData)
    })
}
