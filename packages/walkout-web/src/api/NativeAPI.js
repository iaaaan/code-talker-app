
import uuidv1 from 'uuid/v1'

export default class NativeAPI {
  constructor () {
    this.promiseChain = Promise.resolve()
    this.callbacks = {}

    this.getURL = this.getURL.bind(this)
    this.onMessage = this.onMessage.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    this.log = this.log.bind(this)
    this.onIframeMessage = this.onIframeMessage.bind(this)

    window.addEventListener('message', this.onMessage)

    if (window.ReactNativeWebView) {
      window.addEventListener('iframe-message', this.onIframeMessage)
    }
  }

  onIframeMessage (message) {
    this.log(JSON.stringify(message))
  }

  getURL (url) {
    return url
  }

  log (data) {
    window.ReactNativeWebView.postMessage(JSON.stringify({
      type: 'log',
      data
    }))
  }

  sendMessage (message, callback) {
    if (callback) {
      message._messageId = uuidv1()
    }

    const msg = JSON.stringify(message)
    const callbacks = this.callbacks

    if (window.ReactNativeWebView) {
      this.promiseChain = this.promiseChain.then(function () {
        return new Promise(function (resolve, reject) {
          console.log('sending message ' + JSON.stringify(message))

          if (message._messageId) {
            callbacks[message._messageId] = callback
          }

          window.ReactNativeWebView.postMessage(msg)
          console.log('message posted')

          resolve()
        })
      }).catch(function (e) {
        console.error('message send failed ' + e.message)
      })
    } else {
      console.log('TEST1')
      if (message._messageId) {
        callbacks[message._messageId] = callback
      }

      const messageEvent = new window.CustomEvent('iframe-message', message)
      window.parent.dispatchEvent(messageEvent)
    }
  }

  onMessage (event) {
    if (!event.data) {
      return null
    }

    let message = null

    try {
      message = JSON.parse(event.data)
    } catch (error) {
      this.callbacks[message._messageId]({
        error: 'failed to parse message from react-native ' + error
      })
      return null
    }

    if (!message._messageId) {
      return null
    }

    if (this.callbacks[message._messageId]) {
      this.callbacks[message._messageId](message)
      delete this.callbacks[message._messageId]
    }
  }
}
